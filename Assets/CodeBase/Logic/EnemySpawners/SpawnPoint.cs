﻿using CodeBase.Data;
using CodeBase.Enemy;
using CodeBase.Infastructure.Factory;
using CodeBase.Infastructure.Services.PersistentProgress;
using CodeBase.StaticData;
using UnityEngine;

namespace CodeBase.Logic.EnemySpawners
{
    public class SpawnPoint : MonoBehaviour, ISavedProgress
    {
        public MonsterTypeId MonsterTypeId;
        public bool Slain;

        public string Id { get; set; }
        private IGameFactory _gameFactory;
        private EnemyDeath _enemyDeath;

        public void Construct(IGameFactory gameFactory) => 
            _gameFactory = gameFactory;

        public void LoadProgress(PlayerProgress progress)
        {
            if (progress.KillData.ClearedSpawners.Contains(Id))
                Slain = true;
            else
                Spawn();
        }

        private async void Spawn()
        {
            GameObject monster = await _gameFactory.CreateMonster(MonsterTypeId, transform);
            
            _enemyDeath = monster.GetComponent<EnemyDeath>();
            _enemyDeath.Happened += Slay;
        }

        private void Slay()
        {
            if (_enemyDeath != null)
                _enemyDeath.Happened -= Slay;
            
            Slain = true;
        }

        public void UpdateProgress(PlayerProgress progress)
        {
            if (Slain) 
                progress.KillData.ClearedSpawners.Add(Id);
        }
    }
}