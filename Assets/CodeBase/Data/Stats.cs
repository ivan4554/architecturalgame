﻿using System;

namespace CodeBase.Data
{
    [Serializable]
    public class Stats
    {
        public float DamageRadius;
        public float Damage;
    }
}