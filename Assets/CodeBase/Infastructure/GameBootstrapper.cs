﻿using System;
using System.Collections.Generic;
using CodeBase.Infastructure.States;
using CodeBase.Logic;
using UnityEngine;

namespace CodeBase.Infastructure
{
    public class GameBootstrapper : MonoBehaviour, ICoroutineRunner
    {
        public LoadingCurtain CurtainPrefab;
        private Game _game;
        private void Awake()
        {
            _game = new Game(this,Instantiate(CurtainPrefab));
            _game.StateMachine.Enter<BootstrapState>();
            DontDestroyOnLoad(this);
        }
    }
}