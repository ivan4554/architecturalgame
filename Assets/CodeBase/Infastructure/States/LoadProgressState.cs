﻿using CodeBase.Data;
using CodeBase.Infastructure.Services.PersistentProgress;
using CodeBase.Infastructure.Services.SaveLoad;

namespace CodeBase.Infastructure.States
{
    public class LoadProgressState : IState
    {
        private readonly GameStateMachine _stateMachine;
        private readonly IPersistentProgressService _progressService;
        private ISaveLoadService _saveLoadService;

        public LoadProgressState(GameStateMachine stateMachine,IPersistentProgressService progressService, ISaveLoadService saveLoadService)
        {
            _stateMachine = stateMachine;
            _progressService = progressService;
            _saveLoadService = saveLoadService;
        }

        public void Enter()
        {
            LoadProgressOrInitNew();
            _stateMachine.Enter<LoadLevelState,string>(_progressService.Progress.WorldData.PositionOnLevel.Level);
        }
        
        public void Exit()
        {
        }
        private void LoadProgressOrInitNew() => 
            _progressService.Progress = _saveLoadService.LoadProgress() ?? NewProgress();

        private PlayerProgress NewProgress()
        {
            var progress = new PlayerProgress("Main");
            progress.HeroState.MaxHP = 50;
            progress.HeroStats.Damage = 2;
            progress.HeroStats.DamageRadius = 1f;
            progress.HeroState.ResetHP();
            return progress;
        }
    }
}