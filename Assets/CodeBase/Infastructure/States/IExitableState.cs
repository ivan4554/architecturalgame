﻿namespace CodeBase.Infastructure.States
{
    public interface IExitableState
    {
        void Exit();
    }
}