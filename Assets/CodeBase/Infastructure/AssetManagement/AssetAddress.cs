﻿namespace CodeBase.Infastructure.AssetManagement
{
    public static class AssetAddress
    {
        public const string Spawner = "SpawnPoint";
        public const string Loot = "Loot";
        public const string Hero = "Hero";
        public const string Hud = "Hud";
        public const string LevelTransferTrigger = "LevelTransferTrigger";
    }
}