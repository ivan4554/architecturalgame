﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CodeBase.Enemy;
using CodeBase.Infastructure.Services;
using CodeBase.Infastructure.Services.PersistentProgress;
using CodeBase.StaticData;
using UnityEngine;

namespace CodeBase.Infastructure.Factory
{
    public interface IGameFactory : IService
    {
        Task<GameObject> CreateHero(Vector3 at);
        Task<GameObject> CreateHud();
        List<ISavedProgressReader> ProgressReaders { get; }
        List<ISavedProgress> ProgressWriters { get; }
        void CleanUp();
        Task<GameObject> CreateMonster(MonsterTypeId typeId, Transform parent);
        Task CreateSpawner(string spawnerId, Vector3 at, MonsterTypeId monsterTypeId);
        Task<LootPiece> CreateLoot();
        Task WarmUp();
        Task CreateLevelTransfer(Vector3 at,string transferTo);
    }
}