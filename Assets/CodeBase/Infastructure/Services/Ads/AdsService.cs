﻿using System;
using UnityEngine;
using UnityEngine.Advertisements;

namespace CodeBase.Infastructure.Services.Ads
{
    public class AdsService : IAdsService,  IUnityAdsListener
    {
        private const string IOSGameId = "4488616";
        private const string AndroidGameId = "4488617";
        
        private const string RewardedVideoPlacementId = "ArchitecturalVideo";
        
        public event Action RewardedVideoReady;

        private string _gameId;
        private Action _onVideoFinished;

        public int Reward => 15;

        public void Initialize()
        {
            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                    _gameId = AndroidGameId;
                    break;
                case RuntimePlatform.IPhonePlayer:
                    _gameId = IOSGameId;
                    break;
                case RuntimePlatform.WindowsEditor:
                    _gameId = AndroidGameId;
                    break;
                default:
                    Debug.Log("Unsupported platform for ads");
                    break;
            }
            
            Advertisement.AddListener(this);
            Advertisement.Initialize(_gameId);
        }

        public bool IsRewardedVideoReady => 
            Advertisement.IsReady(RewardedVideoPlacementId);

        public void ShowRewardedVideo(Action onVideoFinished)
        {
            Advertisement.Show(RewardedVideoPlacementId);
            _onVideoFinished = onVideoFinished;
        }

        public void OnUnityAdsReady(string placementId)
        {
            Debug.Log($"OnUnityAdsReady: {placementId}");

            if (placementId == RewardedVideoPlacementId) 
                RewardedVideoReady?.Invoke();
        }

        public void OnUnityAdsDidError(string message)
        {
            Debug.LogError($"OnUnityAdsDidError: {message}");
        }

        public void OnUnityAdsDidStart(string placementId)
        {
            Debug.Log($"OnUnityAdsDidStart: {placementId}");
        }

        public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
        {
            switch (showResult)
            {
                case ShowResult.Failed:
                    Debug.LogError($"OnUnityAdsDidFinish: {showResult}");
                    break;
                case ShowResult.Skipped:
                    Debug.Log($"OnUnityAdsDidFinish: {showResult}");
                    break;
                case ShowResult.Finished:
                    _onVideoFinished?.Invoke();
                    break;
                default:
                    Debug.Log($"OnUnityAdsDidFinish: {showResult}");
                    break;
            }

            _onVideoFinished = null;
        }
    }
}