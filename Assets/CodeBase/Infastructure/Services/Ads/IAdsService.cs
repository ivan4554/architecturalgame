﻿using System;

namespace CodeBase.Infastructure.Services.Ads
{
    public interface IAdsService : IService
    {
        void Initialize();
        bool IsRewardedVideoReady { get; }
        int Reward { get;}
        void ShowRewardedVideo(Action onVideoFinished);
        event Action RewardedVideoReady;
    }
}