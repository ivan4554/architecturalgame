﻿using CodeBase.Data;

namespace CodeBase.Infastructure.Services.PersistentProgress
{
    public interface IPersistentProgressService : IService
    {
        PlayerProgress Progress { get; set; }
    }
}