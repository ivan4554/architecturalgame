﻿using UnityEngine;

namespace CodeBase.Infastructure.Services.Inputs
{
    public interface IInputService : IService
    {
        Vector2 Axes { get; }
        bool IsAttackButton();
    }
}