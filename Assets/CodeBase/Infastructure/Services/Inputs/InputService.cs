﻿using UnityEngine;

namespace CodeBase.Infastructure.Services.Inputs
{
    public abstract class InputService : IInputService
    {
        protected const string Horizontal = "Horizontal";
        protected const string Vertical = "Vertical";
        private const string Button = "Fire";

        public abstract Vector2 Axes { get; }

        public bool IsAttackButton() => 
            SimpleInput.GetButtonUp(Button);
        protected static Vector2 SimpleInputAxes() =>
            new Vector2(SimpleInput.GetAxis(Horizontal), SimpleInput.GetAxis(Vertical));
    }
}