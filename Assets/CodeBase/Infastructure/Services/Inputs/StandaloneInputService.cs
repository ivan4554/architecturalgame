﻿using UnityEngine;

namespace CodeBase.Infastructure.Services.Inputs
{
    public class StandaloneInputService : InputService
    {
        public override Vector2 Axes {
            get
            {
                Vector2 axes = SimpleInputAxes();
                if (axes == Vector2.zero)
                {
                    axes = UnityAxes();
                }

                return axes;
            }
        }
        private static Vector2 UnityAxes()
        {
            return new Vector2(UnityEngine.Input.GetAxis(Horizontal),UnityEngine.Input.GetAxis(Vertical));
        }
    }
}