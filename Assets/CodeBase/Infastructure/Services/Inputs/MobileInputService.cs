﻿using UnityEngine;

namespace CodeBase.Infastructure.Services.Inputs
{
    public class MobileInputService : InputService
    {
        public override Vector2 Axes => SimpleInputAxes();
    }
}