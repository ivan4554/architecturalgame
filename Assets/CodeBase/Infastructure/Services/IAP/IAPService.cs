﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CodeBase.Data;
using CodeBase.Infastructure.Services.PersistentProgress;
using UnityEngine.Purchasing;

namespace CodeBase.Infastructure.Services.IAP
{
    public class IAPService : IIAPService
    {
        private readonly IAPProvider _iapProvider;
        private readonly IPersistentProgressService _progressService;

        public event Action Initialized;
        public bool IsInitialized => _iapProvider.IsInitialized;

        public IAPService(IAPProvider iapProvider, IPersistentProgressService progressService)
        {
            _iapProvider = iapProvider;
            _progressService = progressService;
        }

        public void Initialize()
        {
            _iapProvider.Initialize(this);
            _iapProvider.Initialized += () => Initialized?.Invoke();
        }

        public void StartPurchase(string productId) => 
            _iapProvider.StartPurchase(productId);

        public List<ProductDescription> Products() =>
            ProductsDescriptions().ToList();

        public PurchaseProcessingResult ProcessPurchase(Product purchasedProduct)
        {
            ProductConfig productConfig = _iapProvider.Configs[purchasedProduct.definition.id];

            switch (productConfig.ItemType)
            {
                case ItemType.Skulls:
                    _progressService.Progress.WorldData.LootData.Add(productConfig.Quantity);
                    _progressService.Progress.PurchaseData.AddPurchase(purchasedProduct.definition.id);
                    break;
            }

            return PurchaseProcessingResult.Complete;
        }

        private IEnumerable<ProductDescription> ProductsDescriptions()
        {
            PurchaseData purchaseData = _progressService.Progress.PurchaseData;
            
            foreach (string productId in _iapProvider.Products.Keys)
            {
                ProductConfig config = _iapProvider.Configs[productId];
                Product product = _iapProvider.Products[productId];

                BoughtIAP boughtIap = purchaseData.BoughtIAPs.Find(x => x.IAPId == productId);
                
                if(ProductBoughtOut(boughtIap, config))
                    continue;

                yield return new ProductDescription()
                {
                    Id = productId,
                    Config =  config,
                    Product = product,
                    AvailablePurchasesLeft = boughtIap != null 
                        ? config.MaxPurchaseCount - boughtIap.Count 
                        : config.MaxPurchaseCount
                };
            }
        }

        private static bool ProductBoughtOut(BoughtIAP boughtIap, ProductConfig config) => 
            boughtIap != null && boughtIap.Count >= config.MaxPurchaseCount;
    }
}