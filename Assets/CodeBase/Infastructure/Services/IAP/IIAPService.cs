﻿using System;
using System.Collections.Generic;

namespace CodeBase.Infastructure.Services.IAP
{
    public interface IIAPService : IService
    {
        event Action Initialized;
        bool IsInitialized { get; }
        void Initialize();
        void StartPurchase(string productId);
        List<ProductDescription> Products();
    }
}