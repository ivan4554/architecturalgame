﻿using System;
using System.Collections.Generic;

namespace CodeBase.Infastructure.Services.IAP
{
    [Serializable]
    public class ProductConfigWrapper
    {
        public List<ProductConfig> Configs;
    }
}