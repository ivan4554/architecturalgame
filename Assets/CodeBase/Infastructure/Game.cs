﻿using System;
using System.ComponentModel.Design;
using CodeBase.Infastructure.Services;
using CodeBase.Infastructure.States;
using CodeBase.Logic;
using UnityEngine;

namespace CodeBase.Infastructure
{
    public class Game
    {
        public GameStateMachine StateMachine;

        public Game(ICoroutineRunner coroutineRunner, LoadingCurtain curtain)
        {
            StateMachine = new GameStateMachine(new SceneLoader(coroutineRunner),curtain,AllServices.Container);
        }

        
    }
}