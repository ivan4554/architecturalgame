﻿using CodeBase.Infastructure;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace CodeBase.StaticData
{
    [CreateAssetMenu(fileName = "MonsterData",menuName = "StaticData/Monster")]
    public class MonsterStaticData : ScriptableObject
    {
        public MonsterTypeId MonsterTypeId;
        [Range(1,100)]
        public int Hp;
        [Range(1,30)]
        public float Damage;
        
        [Range(1,5)]
        public int MinLoot;
        [Range(5,20)]
        public int MaxLoot;
        
        [Range(0.5f,1)]
        public float AttackCleavage;

        [Range(0.5f,1)]
        public float AttackEffectiveDistance;

        [Range(1,25)]
        public float MoveSpeed;

        public AssetReferenceGameObject PrefabReference;
    }
}