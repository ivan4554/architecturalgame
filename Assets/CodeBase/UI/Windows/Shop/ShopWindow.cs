﻿using System;
using CodeBase.Infastructure.AssetManagement;
using CodeBase.Infastructure.Services.Ads;
using CodeBase.Infastructure.Services.IAP;
using CodeBase.Infastructure.Services.PersistentProgress;
using TMPro;

namespace CodeBase.UI.Windows.Shop
{
    public class ShopWindow : WindowBase
    {
        public TextMeshProUGUI SkullText;
        public RewardedAdItem AdItem;
        public ShopItemsContainer ShopItemsContainer;
        public void Construct(IAdsService adsService, IPersistentProgressService progressService,
            IIAPService iapService, IAssets assets)
        {
            base.Construct(progressService);
            AdItem.Construct(adsService, progressService);
            ShopItemsContainer.Construct(iapService, progressService, assets);
        }

        protected override void Initialize()
        {
            AdItem.Initialize();
            RefreshSkullText();
            ShopItemsContainer.Initialize();
        }

        protected override void SubscribeUpdates()
        {
            AdItem.Subscribe();
            Progress.WorldData.LootData.Changed += RefreshSkullText;
            ShopItemsContainer.Subscribe();
        }

        protected override void CleanUp()
        {
            base.CleanUp();
            AdItem.CleanUp();
            ShopItemsContainer.CleanUp();
            Progress.WorldData.LootData.Changed -= RefreshSkullText;
        }

        private void RefreshSkullText() => 
            SkullText.text = Progress.WorldData.LootData.Collected.ToString();
    }
}