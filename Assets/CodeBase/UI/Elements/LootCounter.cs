﻿using CodeBase.Data;
using CodeBase.Infastructure.Services.PersistentProgress;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CodeBase.UI.Elements
{
    public class LootCounter : MonoBehaviour, ISavedProgress
    {
        public TextMeshProUGUI Counter;
        private WorldData _worldData;

        public void Construct(WorldData worldData)
        {
            _worldData = worldData;
            _worldData.LootData.Changed += UpdateCounter;

            UpdateCounter();
        }

        public void LoadProgress(PlayerProgress progress)
        {
            if (CurrentLevel() == progress.WorldData.PositionOnLevel.Level) 
                UpdateCounter();
        }

        public void UpdateProgress(PlayerProgress progress) => 
            progress.WorldData.LootData.Collected = int.Parse(Counter.text);

        private void UpdateCounter() => 
            Counter.text = $"{_worldData.LootData.Collected}";

        private static string CurrentLevel() => 
            SceneManager.GetActiveScene().name;
    }
}