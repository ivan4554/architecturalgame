﻿using System.Threading.Tasks;
using CodeBase.Infastructure.AssetManagement;
using CodeBase.Infastructure.Services;
using CodeBase.Infastructure.Services.Ads;
using CodeBase.Infastructure.Services.IAP;
using CodeBase.Infastructure.Services.PersistentProgress;
using CodeBase.StaticData;
using CodeBase.UI.Services.Windows;
using CodeBase.UI.Windows;
using CodeBase.UI.Windows.Shop;
using UnityEngine;

namespace CodeBase.UI.Services.Factory
{
    public class UIFactory : IUIFactory
    {
        private const string UIRootPath = "UIRoot";
        private readonly IAssets _assets;
        private readonly IStaticDataService _staticData;
        private Transform _uiRoot;
        private readonly IPersistentProgressService _progressService;
        private readonly IAdsService _adsService;
        private readonly IIAPService _iapService;

        public UIFactory(IAssets assets, IStaticDataService staticData, IPersistentProgressService progressService, IAdsService adsService, IIAPService iapService)
        {
            _assets = assets;
            _staticData = staticData;
            _progressService = progressService;
            _adsService = adsService;
            _iapService = iapService;
        }

        public void CreateShop()
        {
            WindowConfig config = _staticData.ForWindow(WindowId.Shop);

            ShopWindow window = Object.Instantiate(config.Prefab, _uiRoot) as ShopWindow;
            window.Construct(_adsService,_progressService, _iapService, _assets);
        }

        public async Task CreateUIRoot()
        {
            var root = await _assets.Instantiate(UIRootPath);
            _uiRoot = root.transform;
        }
    }
}