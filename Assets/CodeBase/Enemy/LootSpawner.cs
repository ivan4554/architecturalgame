﻿using System;
using System.Threading.Tasks;
using CodeBase.Data;
using CodeBase.Infastructure.Factory;
using CodeBase.Logic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CodeBase.Enemy
{
    public class LootSpawner : MonoBehaviour
    {
        public EnemyDeath EnemyDeath;
        private IGameFactory _gameFactory;
        private int _minLoot;
        private int _maxLoot;

        public void Construct(GameFactory gameFactory) => 
            _gameFactory = gameFactory;

        private void Start() => 
            EnemyDeath.Happened += SpawnLoot;

        private async void SpawnLoot()
        {
            LootPiece lootPiece =  await _gameFactory.CreateLoot();
            lootPiece.transform.position = transform.position;
            lootPiece.GetComponent<UniqueId>().GenerateId();
            
            Loot lootItem = GenerateLoot();
            lootPiece.Initialize(lootItem);
        }

        public void SetLoot(int min, int max)
        {
            _minLoot = min;
            _maxLoot = max;
        }

        private Loot GenerateLoot()
        {
            return new Loot()
            {
                Value = Random.Range(_minLoot,_maxLoot)
            };
        }
    }
}